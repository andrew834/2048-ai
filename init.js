var grid = new Grid();

var tileWidth = 200;
var tileHeight = 200;

var c = document.getElementById("mycanvas");
c.width = window.innerWidth;
c.height = window.innerHeight;
var ctx = c.getContext("2d");
ctx.font = "bold 60px sans-serif";

grid.initGrid();
draw();
