document.onkeydown = function(e) {
	e.preventDefault();
	if (!grid.gameOver) {
		if (e.keyCode == "38") {
			if (grid.moveUp())
				grid.spawnRandomNumber();
		} else if (e.keyCode == "40") {
			if (grid.moveDown())
				grid.spawnRandomNumber();
		} else if (e.keyCode == "37") {
			if (grid.moveLeft())
				grid.spawnRandomNumber();
		} else if (e.keyCode == "39") {
			if (grid.moveRight())
				grid.spawnRandomNumber();
		}
		grid.isGameOver();
		draw();
	}
}