function Grid() {
	/*this.grid = [
		[new Tile(2), new Tile(1024), new Tile(8), new Tile(4)],
		[new Tile(16), new Tile(256), new Tile(8), new Tile(128)],
		[new Tile(128), new Tile(64), new Tile(32), new Tile(4)],
		[new Tile(4), new Tile(8), new Tile(4), new Tile(2)]
	];*/
	this.grid = [
		[null, null, null, null],
		[null, null, null, null],
		[null, null, null, null],
		[null, null, null, null]
	];
	this.gameOver = false;
}

Grid.prototype.emptyGrid = function() {
	return [
		[null, null, null, null],
		[null, null, null, null],
		[null, null, null, null],
		[null, null, null, null]
	];
}


Grid.prototype.initGrid = function() {
	this.spawnRandomNumber();
	this.spawnRandomNumber();
}

Grid.prototype.getEmptySpots = function() {
	var emptySpots = [];
	for (var i = 0; i < 4; i++) {
		for (var j = 0; j < 4; j++) {
			if (this.grid[i][j] === null) {
				emptySpots.push({
					i: i,
					j: j
				});
			}
		}
	}
	return emptySpots;
}

Grid.prototype.spawnRandomNumber = function() {
	var emptySpots = this.getEmptySpots();
	if (emptySpots.length == 0) {
		return;
	}
	var number = (Math.random() < 0.9) ? 2 : 4;
	var randomSpotIndex = Math.floor(Math.random() * emptySpots.length);
	var spot = emptySpots[randomSpotIndex];
	this.grid[spot.i][spot.j] = new Tile(number);
}

Grid.prototype.moveUp = function() {
	var changed = false;
	var mergedArray = [];
	for (var i = 1; i < 4; i++) {
		for (var j = 0; j < 4; j++) {
			if (this.grid[i][j] !== null) {
				var ret = this.calcPositionUp(i, j, this.grid[i][j].value);
				if (ret !== null) {
					changed = true;
					if (ret.hasOwnProperty("y")) {
						mergedArray.push(ret);
					}
				}
			}
		}
	}

	if (changed) {
		for (var i = 0; i < mergedArray.length; i++) {
			var t = mergedArray[i];
			this.grid[t.y][t.x].wasMerged = false;
		}
	}

	return changed;
}

Grid.prototype.calcPositionUp = function(y, x, number) {
	var boardChanged = null;
	var oldY = y;
	while (y--) {

		if (this.grid[y][x] === null) {
			this.grid[y][x] = new Tile(number);
			this.grid[oldY][x] = null;
			oldY = y;
			boardChanged = {
				x: -1
			};
		} else {
			if (this.grid[y][x].value == number && !this.grid[y][x].wasMerged) {
				this.grid[y][x].value += number;
				this.grid[y][x].wasMerged = true;
				this.grid[oldY][x] = null;
				return {
					x: x,
					y: y
				};
			}

			if (this.grid[y][x] !== null && this.grid[y][x].value != number) {
				break;
			}
		}
	}
	return boardChanged;
}

Grid.prototype.moveDown = function() {
	var changed = false;
	var mergedArray = [];

	for (var i = 2; i >= 0; i--) {
		for (var j = 0; j < 4; j++) {
			if (this.grid[i][j] !== null) {
				var ret = this.calcPositionDown(i, j, this.grid[i][j].value);
				if (ret !== null) {
					changed = true;
					if (ret.hasOwnProperty("y")) {
						mergedArray.push(ret);
					}
				}
			}
		}
	}

	if (changed) {
		for (var i = 0; i < mergedArray.length; i++) {
			var t = mergedArray[i];
			this.grid[t.y][t.x].wasMerged = false;
		}
	}

	return changed;
}

Grid.prototype.calcPositionDown = function(y, x, number) {
	var boardChanged = null;
	var oldY = y;
	while (y < 3) {
		y++;
		if (this.grid[y][x] === null) {
			this.grid[y][x] = new Tile(number);
			this.grid[oldY][x] = null;
			oldY = y;
			boardChanged = {
				x: -1
			};
		} else {
			if (this.grid[y][x].value == number && !this.grid[y][x].wasMerged) {
				this.grid[y][x].value += number;
				this.grid[oldY][x] = null;
				this.grid[y][x].wasMerged = true;
				return {
					x: x,
					y: y
				};
			}

			if (this.grid[y][x] !== null && this.grid[y][x].value != number) {
				break;
			}
		}
	}
	return boardChanged;
}

Grid.prototype.moveRight = function() {
	var changed = false;
	var mergedArray = [];

	for (var i = 0; i < 4; i++) {
		for (var j = 2; j >= 0; j--) {
			if (this.grid[i][j] !== null) {
				var ret = this.calcPositionRight(i, j, this.grid[i][j].value);
				if (ret !== null) {
					changed = true;
					if (ret.hasOwnProperty("x")) {
						mergedArray.push(ret);
					}
				}
			}
		}
	}

	if (changed) {
		for (var i = 0; i < mergedArray.length; i++) {
			var t = mergedArray[i];
			this.grid[t.y][t.x].wasMerged = false;
		}
	}

	return changed;
}

Grid.prototype.calcPositionRight = function(y, x, number) {
	var boardChanged = null;
	var oldX = x;
	while (x < 3) {
		x++;
		if (this.grid[y][x] === null) {
			this.grid[y][x] = new Tile(number);
			this.grid[y][oldX] = null;
			oldX = x;
			boardChanged = {
				y: -1
			};
		} else {
			if (this.grid[y][x].value == number && !this.grid[y][x].wasMerged) {
				this.grid[y][x].value += number;
				this.grid[y][oldX] = null;
				this.grid[y][x].wasMerged = true;
				return {
					x: x,
					y: y
				};
			}

			if (this.grid[y][x] !== null && this.grid[y][x].value != number) {
				break;
			}
		}
	}
	return boardChanged;
}

Grid.prototype.moveLeft = function() {
	var changed = false;
	var mergedArray = [];
	for (var i = 0; i < 4; i++) {
		for (var j = 1; j < 4; j++) {
			if (this.grid[i][j] !== null) {
				var ret = this.calcPositionLeft(i, j, this.grid[i][j].value);
				if (ret !== null) {
					changed = true;
					if (ret.hasOwnProperty("x")) {
						mergedArray.push(ret);
					}
				}
			}
		}
	}

	if (changed) {
		for (var i = 0; i < mergedArray.length; i++) {
			var t = mergedArray[i];
			this.grid[t.y][t.x].wasMerged = false;
		}
	}

	return changed;
}

Grid.prototype.calcPositionLeft = function(y, x, number) {
	var boardChanged = null;
	var oldX = x;
	while (x--) {
		if (this.grid[y][x] === null) {
			this.grid[y][x] = new Tile(number);
			this.grid[y][oldX] = null;
			oldX = x;
			boardChanged = {
				y: -1
			};
		} else {
			if (this.grid[y][x].value == number && !this.grid[y][x].wasMerged) {
				this.grid[y][x].value += number;
				this.grid[y][oldX] = null;
				this.grid[y][x].wasMerged = true;
				return {
					x: x,
					y: y
				};
			}
			if (this.grid[y][x] !== null && this.grid[y][x].value != number) {
				break;
			}
		}
	}
	return boardChanged;
}

Grid.prototype.isGameOver = function() {
	this.gameOver = true;
	for (var i = 0; i < 4; i++) {
		for (var j = 0; j < 4; j++) {
			if (this.grid[i][j] === null || this.hasNeighbour(i, j, this.grid[i][j].value)) {
				this.gameOver = false;
				return;
			}
		}
	}
}

Grid.prototype.hasNeighbour = function(i, j, number) {
	if (j + 1 <= 3 && this.grid[i][j + 1] !== null) {
		if (this.grid[i][j + 1].value == number) {
			return true;
		}
	}
	if (i + 1 <= 3 && this.grid[i + 1][j]) {
		if (this.grid[i + 1][j].value == number) {
			return true;
		}
	}

	return false;
}

Grid.prototype.resetMerge = function() {
	for (var i = 0; i < 4; i++) {
		for (var j = 0; j < 4; j++) {
			if (this.grid[i][j] !== null) {
				this.grid[i][j].wasMerged = false;
			}
		}
	}
}

Grid.prototype.copy = function() {
	var newGrids = new Grid();
	newGrids.grid = newGrids.emptyGrid();

	for (var i = 0; i < 4; i++) {
		for (var j = 0; j < 4; j++) {
			if (this.grid[i][j]) {
				newGrids.grid[i][j] = new Tile(this.grid[i][j].value);
			}
		}
	}

	return newGrids;
}

Grid.prototype.gridToString = function() {
	var str = "";
	for (var i = 0; i < 4; i++) {
		for (var j = 0; j < 4; j++) {
			if (this.grid[i][j] === null) {
				str += " " + 0;
			} else {
				str += " " + this.grid[i][j].value;
			}
		}
		str += "\n";
	}
	return str;
}