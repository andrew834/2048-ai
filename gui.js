function draw() {
	for (var i = 0; i < 4; i++) {
		for (var j = 0; j < 4; j++) {
			x = j * tileWidth;
			y = i * tileHeight;
			if (i == 0) {
				y += 10;
			}
			if (j == 0) {
				x += 10;
			}
			ctx.fillStyle = "#424242";
			ctx.fillRect(x, y, tileWidth, tileHeight);
			ctx.lineWidth = 5;
			ctx.strokeRect(x, y, tileWidth, tileHeight);
			if (grid.grid[i][j] !== null) {
				drawNumber(grid.grid[i][j].value, x, y);
			}
		}
	}
}

function drawNumber(n, x, y) {
	x += tileWidth / 2;
	y += tileHeight / 2;

	if (n < 10) {
		ctx.fillStyle = "#F2EFDC";
	} else if (n < 100) {
		ctx.fillStyle = "#F2DBA7";
	} else if (n < 1000) {
		ctx.fillStyle = "#F2B47D";
	} else if (n < 10000) {
		ctx.fillStyle = "#F28247";
	}
	x -= ctx.measureText(String(n)).width / 2;
	y += 15;

	ctx.fillText(n, x, y);
}